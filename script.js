const gameContainer = document.getElementById("game");
const mainContainer = document.querySelector(".main-container");
const message = document.querySelector("h2");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!

let counter = 0;
let clickOne = "";
let clickTwo = "";
let matches = 0;

function handleCardClick(event) {

  // you can use event.target to see which element was clicked
  let changeValue = event.target;
  if (counter < 2) {
    if (changeValue.style.backgroundColor === "" || changeValue.style.backgroundColor === "white") {
      console.log("hello");
      if (counter === 0) {
        clickOne = event.target;
        counter++;
      } else {
        clickTwo = event.target;
        counter++;
      }
    }

    if (clickOne.className === clickTwo.className) {
      clickOne.style.backgroundColor = event.target.className;
      clickTwo.style.backgroundColor = event.target.className;
      matches++;
      counter = 0;

    } else {
      changeValue.style.backgroundColor = event.target.className;
      if (counter === 2) {
        setTimeout(() => {
          changeValue.style.backgroundColor = "white";
          clickOne.style.backgroundColor = "white";
          counter = 0;
        }, 1 * 1000);
        clickTwo = "";
      }
    }
    if (matches == 5) {
      mainContainer.style.display = "none";
      message.style.display = "block";
    }
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);
